import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js' ;
import { ConnectGplus, IConnectGplus } from '../models/connect-gplus.model';

@Injectable()
export class StorageHelper {

  private static _instance: StorageHelper;

  constructor() { }

  public static get Instance() {
    return this._instance || (this._instance = new this());
  }

  public generateSalt(): string {
    let text = '';
    // tslint:disable-next-line:prefer-const
    let possible = '@ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!&$#%&';

    for ( let i = 0; i < 10; i++ ) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
  }

  public saveSessionStorage ( key_: string, value_: Object ): void {
    if ( value_) {
        const _salt = this.generateSalt();
        const ciphertext = CryptoJS.AES.encrypt( JSON.stringify(value_) , _salt );
        window.sessionStorage.setItem( key_, _salt + ciphertext.toString() );
    }else {
        window.sessionStorage.removeItem( key_ );
    }
  }

  public getSessionStorage ( key_: string ): Object {
    if ( window.sessionStorage[ key_ ] ) {
        const _storage_value: string = window.sessionStorage[ key_ ];
        const _salt = _storage_value.substr(0, 10 );
        const _value = _storage_value.substr(10, _storage_value.length );
        const bytes = CryptoJS.AES.decrypt( _value, _salt );
        return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
    }
  }

  public getContext(name_storage: string): any {
    return this.getSessionStorage(name_storage) as any;
  }

  public delContext(name_storage: string): any {
      return window.sessionStorage.removeItem(name_storage);
  }

  protected isLogged(name_storage: string): boolean {

    const ctx: any = this.getContext(name_storage);
    if ( ctx && ctx.token ) {
        return true;
    }
    return false;
  }

}
