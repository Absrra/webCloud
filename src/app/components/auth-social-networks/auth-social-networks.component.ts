import { Component, OnInit, AfterViewInit, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, Params, NavigationEnd } from '@angular/router';

import { StorageHelper } from '../../helpers/storage.helper.service';

import { IConnectGplus, ConnectGplus } from '../../models/connect-gplus.model';

declare const gapi: any;
@Component({
  selector: 'app-auth-social-networks',
  templateUrl: './auth-social-networks.component.html',
  styleUrls: ['./auth-social-networks.component.scss']
})
export class AuthSocialNetworksComponent implements OnInit, AfterViewInit {

  /* Variable que almacenará el token de inicio de sesion en google+ */
  /**
   * Los nombres de las variables de los session Storage son:
   * Gplus
   * Ytube
   */
  public session_gplus: IConnectGplus = new ConnectGplus();


  /* Variable para controlar los step de las redes así como el icono de la red social */
  public step: String = 'g-1';
  public step_icon: String  = 'Googleplus';

  /* Variables para conectar con la api de G+*/
  public auth2: any;
  private cliente_id: String = '682846398783-uku65ftf2t11cietb21bai6v123fg9qn.apps.googleusercontent.com';
  private scope = [
    'profile',
    'email',
    'https://www.googleapis.com/auth/plus.me',
    'https://www.googleapis.com/auth/plus.login',
    'https://www.googleapis.com/auth/contacts.readonly',
    'https://www.googleapis.com/auth/admin.directory.user.readonly'
  ].join(' ');

  /* Variables para la captura de parémetros por url */
  public sub: any;
  public params_: any;

  constructor(
    private element: ElementRef,
    private router: Router,
    private route: ActivatedRoute
  ) {

  }

  ngOnInit() {
    console.log('init', StorageHelper.Instance.getContext('Gplus'));
    this.sub  = this.route.params.subscribe((_params) => {
      this.params_  = _params['id'];
      if (this.params_ !== undefined) {
        this.step = this.params_;
        console.log('hay algo');
      }
    });

  }

  /**
   * nextStep
   */
  public nextStep(next: String) {
    console.log('next');
    this.step = next;
  }

  /**
  * googleInit
  */
  public googleInit() {

    gapi.load('auth2', () => {
      this.auth2  = gapi.auth2.init({
        client_id: this.cliente_id,
        cookiepolicy: 'single_host_origin',
        scope: this.scope
      });
      this.attachSignin(this.element.nativeElement.firstChild);
    });
  }

  public attachSignin(element: any) {
    this.auth2.attachClickHandler('googleBtn', {}, (response: any) => {
      this.session_gplus.El  = response.El;
      this.session_gplus.tokens    = response.Zi;
      this.session_gplus.data  = response.w3;
      StorageHelper.Instance.saveSessionStorage('Gplus', this.session_gplus);
      console.log(StorageHelper.Instance.getContext('Gplus'));
    });

  }


  ngAfterViewInit() {
    this.googleInit();
  }
}
